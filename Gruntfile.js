module.exports = function(grunt) {

  grunt.initConfig({
    distFolder : 'dist',
    concat : {
        options:{
           separator : ";",
        },
        dist:{
           src : ['scripts/app.js','scripts/directives/*.js'],
           dest : '<%= distFolder %>/dxiChart.js'
        }
    },
    uglify:{
       js:{
         files:{
            "<%= distFolder %>/dxiChart.js":['<%= distFolder %>/dxiChart.js']
         }
       }
    },
    html2js:{
       options:{

       },
       main:{
          src: ['views/*.html'],
          dest:'<%= distFolder %>/template.js'
       }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-html2js');

  grunt.registerTask('build', ['concat','uglify','html2js']);
  

};