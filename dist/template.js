angular.module('templates-main', ['../views/doubleDonut.html', '../views/dxiMixChart.html', '../views/pie.html']);

angular.module("../views/doubleDonut.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../views/doubleDonut.html",
    "<div class=\"dxi-chart pie-container\">\n" +
    "   	 \n" +
    "     <div class=\"data-wrapper\" ng-mouseleave=\"mouseLeaveCallBack($event)\"> \n" +
    "   	  <div class=\"data-detail\">\n" +
    "         <strong><nobr>{{currentTip.chartInfo.originEnd - currentTip.chartInfo.originStart}}% {{currentTip.label}}</nobr></strong><br>\n" +
    "         <span><nobr>{{currentTip.value}} seconds</nobr></span>\n" +
    "      </div>\n" +
    "   	  <span class=\"glyphicon glyphicon-zoom-in{{currentTip.chartInfo.zoomIn ? ' glyphicon-zoom-out':''}}\" ng-click=\"toogleZoomIn()\"></span>\n" +
    "     </div>\n" +
    "\n" +
    "     <svg id=\"svg_donut\" ng-attr-width=\"{{config.ROutO*2+2}}\" ng-attr-height=\"{{config.ROutO*2+2}}\" ng-mouseleave=\"mouseLeaveCallBack($event)\">\n" +
    "         <g class=\"out-donut\">\n" +
    "            <path ng-repeat=\"d in data.basic\" ng-attr-d=\"{{arc(d,true)}}\" ng-attr-fill=\"{{d.chartInfo.color}}\" stroke=\"white\" stroke-width=\"{{d.chartInfo.border}}\" ng-attr-transform=\"translate({{config.ROutO}},{{config.ROutO}})\"\n" +
    "            ng-mouseenter=\"locateZoomInIcon(d,config.ROutO,config.ROutI,true)\"\n" +
    "            >\n" +
    "            </path>\n" +
    "         </g>\n" +
    "\n" +
    "         <g class=\"inner-donut\">\n" +
    "            <path ng-repeat=\"d in data.sub\" ng-attr-d=\"{{arc(d,false)}}\" \n" +
    "             ng-attr-fill=\"{{d.chartInfo.color}}\" stroke=\"white\" stroke-width=\"{{d.chartInfo.border}}\" ng-attr-transform=\"translate({{config.ROutO}},{{config.ROutO}})\"\n" +
    "             ng-mouseenter=\"locateZoomInIcon(d,config.RInO,config.RInI,true)\"\n" +
    "             ></path>\n" +
    "         </g>\n" +
    "     </svg>\n" +
    "   </div>");
}]);

angular.module("../views/dxiMixChart.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../views/dxiMixChart.html",
    "<div class=\"dxi-chart\">\n" +
    "\n" +
    "<div class=\"data-wrapper\" ng-mouseleave=\"mouseLeaveCallBack($event)\"> \n" +
    "      <div class=\"data-detail\">\n" +
    "         <strong><nobr> {{curretAgent.label}} </nobr></strong><br>\n" +
    "         <div>\n" +
    "          <nobr> \n" +
    "            <label>{{curretAgent.key}} : {{curretAgent.value}}</label>\n" +
    "\n" +
    "          </nobr>\n" +
    "         </div>\n" +
    "      </div>\n" +
    "</div>\n" +
    "\n" +
    "<svg ng-attr-width=\"{{svgWidth}}\" ng-attr-height=\"{{svgHeight}}\">\n" +
    "  \n" +
    "   <g class=\"axis y grid\"></g>\n" +
    "   <g class=\"axis x grid\"></g>\n" +
    "   <g class=\"bar-group\">\n" +
    "   	  <g ng-repeat=\"agents in barData\">\n" +
    "           <g ng-repeat=\"agent in agents\">\n" +
    "               <rect ng-repeat=\"(key,bar) in agent.values\" \n" +
    "                ng-attr-x=\"{{bar.x}}\"\n" +
    "                ng-attr-y=\"{{bar.y}}\"\n" +
    "                ng-attr-fill=\"{{bar.color}}\"\n" +
    "                ng-attr-width=\"{{bar.width}}\"\n" +
    "                ng-attr-height=\"{{bar.height}}\"\n" +
    "                ng-mouseenter = \"mouseOverCallBack(agent,bar,key,$event)\"\n" +
    "                ng-mouseleave = \"mouseLeaveCallBack()\";\n" +
    "               >\n" +
    "               </rect>\n" +
    "           </g>\n" +
    "   	  </g>\n" +
    "   </g>\n" +
    "   \n" +
    "   <g class=\"path-group\">\n" +
    "\n" +
    "   </g>\n" +
    "\n" +
    "   <g class=\"dots-group\">\n" +
    "      \n" +
    "   </g>\n" +
    "\n" +
    "\n" +
    "\n" +
    "   <g class=\"context\">\n" +
    "      <g class=\"path-group\"></g>\n" +
    "      <g class=\"outside-brush\"></g>\n" +
    "      <g class=\"outside-brush-border\"></g>\n" +
    "      <g class=\"x brush\"> </g>\n" +
    "      <g class=\"border-lines\"> </g>\n" +
    "      <g class=\"split-lines\"></g>\n" +
    "      <g class=\"x axis\"></g>\n" +
    "   </g>\n" +
    "\n" +
    "</svg>\n" +
    "\n" +
    "\n" +
    "</div>");
}]);

angular.module("../views/pie.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../views/pie.html",
    "<svg class=\"dxi-chart\" ng-attr-width=\"{{width}}\" ng-attr-height=\"{{height}}\">\n" +
    "   \n" +
    "   <g ng-attr-transform=\"translate({{width/2}},{{height/2}})\">\n" +
    "      <path ng-attr-d=\"{{arc(d)}}\" ng-repeat=\"d in pieData track by $index\" ng-attr-fill=\"{{getColor($index)}}\" ng-mouseenter=\"mouseEnterCallBack(d,$event,$index)\" ng-mouseout=\"mouseLeaveCallBack($event)\">\n" +
    "      </path>\n" +
    "   </g>\n" +
    "\n" +
    "\n" +
    "   <g id=\"call-detail\" opacity=\"1\" class=\"hideChartElement\">\n" +
    "   	<rect x=\"0\" y=\"0\" width=\"100\" height=\"69\" style=\"stroke:#d8dadc;stroke-width:1;fill:white\"></rect>\n" +
    "   	<circle cx=\"15\" cy=\"34\" r=\"4\" fill=\"{{dataDetail.color}}\"></circle>\n" +
    "   	\n" +
    "   	<text x=\"29\" y=\"26\" style=\"color:#666666;font-size:14px\">\n" +
    "       {{dataDetail.label}}\n" +
    "   	</text>\n" +
    "\n" +
    "   	<text x=\"29\" y=\"52\" style=\"color:#666666;font-size:14px\">\n" +
    "   		{{dataDetail.value}} Calls\n" +
    "   	</text>\n" +
    "    </g>\n" +
    "\n" +
    " </svg>\n" +
    "\n" +
    "");
}]);
