<!doctype html>
<html>
   <head>
       <title> Chart Experiments</title>
       <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css"/>
       <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css"/>
       <link rel="stylesheet" type="text/css" href="styles/callChart.css"/>
   </head>
   <body ng-app="chart" ng-controller="main">

  <p>&nbsp;</p>
  <div class="container">
  	  
      <div class="row">
          <dxi-mix-chart data="newCallStructure"></dxi-mix-chart>
      </div>


  </div>


 <div class="container">
   <div class="row">
      
      <div class="col-lg-6 col-md-6">
          
          <pie data="pieChart" setting="pieSetting"></pie>

      </div>

      <div class="col-lg-6 col-md-6">
            
            <div class="btn btn-primary" ng-click="addPieData()">Add Data</div>
            <p>&nbsp;</p>
            <div class="btn btn-primary" ng-click="removePieData()">Remove Data</div>

      </div>
    
   </div>
  

   <div class="row">
       
       <double-donut data="agentData" size="'large'"></double-donut>
       <double-donut data="agentData2" size="'small'"></double-donut>

   </div>

 </div>

   <script type="text/javascript" src="bower_components/angular/angular.min.js">
   </script>
   <script type="text/javascript" src="bower_components/angular-classy/angular-classy.min.js">
   </script>
   <script type="text/javascript" src="bower_components/d3/d3.min.js">
   </script>
   <script type="text/javascript" src="bower_components/jquery/dist/jquery.js">
   </script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js">
   </script>
   <script type="text/javascript" src="bower_components/underscore/underscore-min.js">
   </script>
    
   <!-- inlude angular app.js -->
   <script type="text/javascript" src="scripts/app.js"></script>
 
   <!-- include angular controller -->
   <script type="text/javascript" src="scripts/controllers/main.js"></script>

   <!-- include angular directives -->
   <script type="text/javascript" src="scripts/directives/pie.js"></script>
   <script type="text/javascript" src="scripts/directives/doubleDonut.js"></script>
   <script type="text/javascript" src="scripts/directives/dxiMixChart.js"></script>
   

   </body>
</html>