"use strict";

angular.module("chart").classy.controller({
	name:"main",
	
	inject:['$scope'],
	
	init:function(){

		this.$.callData = [];

    var basicTime = 1430218102562;
    var ondayTime = 1000 * 60 * 60 * 24; 
    var max = 2;
    this.$.newCallStructure = [];

    for(var i=0;i<30;i++){
        this.$.newCallStructure.push(
          {
            total: Math.round(Math.random()*max) + 2,
            compelete: Math.round(Math.random()*max) + 2,
            date : basicTime + ondayTime * i,
            label : "daniel"
          }
        );

        this.$.newCallStructure.push(
          {
            total: Math.round(Math.random()*max) + 1,
            compelete: Math.round(Math.random()*max) + 2,
            date : basicTime + ondayTime * i,
            label : "alex"
          }
        );

        this.$.newCallStructure.push(
          {
            total: Math.round(Math.random()*max) + 1,
            compelete: Math.round(Math.random()*max) + 2,
            date : basicTime + ondayTime * i,
            label : "frimino"
          }
        );
    }


		//this.$.callData.push(this.$.addAgents("get"));

		this.$.agents=["agent"];

        //this.generateTotalAndCompeleteCalls();
		
        this.$.pieChart = [
           {label:"agent1",count:10},
           {label:"agent2",count:20}
        ];




  this.$.agentData = {
       
       basic:[{
            label: "Talk",
            value : 70
         },
         {
            label : "Idle",
            value : 8
        },
        {
            label : "Avilable",
            value : 10
        },
        ],
        
        sub : [
           {
            label:"Compelete",
            value: 80
           },
           {
            label:"Sale",
            value: 40
           },
        ]
	};

  this.$.agentData2 = {
       
       basic:[{
            label: "Talk",
            value : 6
         },
         {
            label : "Idle",
            value : 8
        },
        {
            label : "Avilable",
            value : 10
        },
        ],
        
        sub : [
           {
            label:"Compelete",
            value: 20
           },
           {
            label:"Sale",
            value: 30
           },
        ]
  };

  this.$.pieSetting={
     width:200,
     height:200,
     innerRadius:30
  }



 },

  
	methods:{

	  generateTotalAndCompeleteCalls:function(){
        //this.$.total.totalCalls
        var that = this;
        
        /**
        * define the value of total
        **/
        this.$.total = {
           total:[],
           compelete:[]
        };


        this.$.callDataNew.map(function(v,index){
             v.map(function(v2,index2){
                  if(!that.$.total.total[index2]){
                  	 that.$.total.total[index2] = 0;
                  }
                  that.$.total.total[index2]+=v2.total;

                  if(!that.$.total.compelete[index2]){
                  	 that.$.total.compelete[index2] = 0;
                  }
                  that.$.total.compelete[index2]+=v2.compelete;
             });             
        }); 

     },	

	  updateData:function(){
        console.log("update the chart data");
        this.$.callData = [[20,20,30,40,50,60,70,80]];  
        //this.$.callData[0][0] = 40;
      },
 
      addAgents:function(get){
          if(this.$.callData.length >=7 && !get) return false;
          var data = [];
          for(var i=0;i<this.$.codinateXlabels.length;i++){
             var y = 0;
             switch(i){
             	case 0:
             	case this.$.codinateXlabels.length-1:
             	   y = 2;
             	   break;
             	default:
             	   y = Math.round(Math.random()*100)
             }
             data.push(y);
          }
          if(get) return data;
          this.$.callData.push(data);
          this.$.agents.push("agent"+this.$.callData.length);
          //this.generateTotalAndCompeleteCalls();
      },

      removeAgents:function(){
          this.$.callData.pop();
          this.$.agents.pop();
      },

      addPieData:function(){
      	  var label = "agent" + (this.$.pieChart.length + 1);
      	  var value = Math.round(Math.random()*100);
      	  this.$.pieChart.push({label:label,count:value});
      },

      removePieData:function(){
          this.$.pieChart.pop();
      },

      changeCodinateXlabels : function(){
           this.$.codinateXlabels = ["2014-03-15","2014-03-16"];
      },

      getNewData : function(){
           var startDate = new Date(jQuery("#startDatepicker").val());
           var endDate = new Date(jQuery("#endDatepicker").val());
           var timeGap = jQuery("#timegap").val();
           
           var hour = 60*60*1000;
           var day = hour * 24;
           var month = day*30;
           var timegap = 0;

           if(timeGap==0){
           	  alert("timegap can not be 0");
           	  return false;
           }

           switch(timeGap){
           	   case "month":
           	        timegap = month;
           	        break;
           	   case "day":
           	        timegap = day;
           	        break;
           	   case "hour":
           	        timegap = hour;
           	        break;
           }

           if(endDate.getTime() - startDate.getTime() < 0){
           	  alert("end date must be later then start date");
           	  return false;
           }



           var points = Math.floor((endDate.getTime() - startDate.getTime())/timegap);
           
           console.log("the value of points is:",points);
         
           
           var codinateXlabels = [];
           
           codinateXlabels.push(startDate.getTime());
            for(var i=1;i<=points;i++){
                codinateXlabels.push(startDate.getTime()+timegap*i - timegap/2);
            };

           codinateXlabels.push(endDate.getTime());
   
           this.$.codinateXlabels = codinateXlabels;

           var newCallData = [];
           
           for(var i=0; i< this.$.agents.length;i++){
           	   newCallData.push(this.$.addAgents("get"));
           }

           this.$.callData = newCallData;
           //this.generateTotalAndCompeleteCalls();
           console.log(startDate,endDate,timeGap,timegap);
      },

      getNewMultipleData : function(){
           var startDate = new Date(jQuery("#startDatepicker2").val());
           var endDate = new Date(jQuery("#endDatepicker2").val());
           var timeGap = jQuery("#timegap2").val();
           
           var hour = 60*60*1000;
           var day = hour * 24;
           var month = day*30;
           var timegap = 0;

           if(timeGap==0){
           	  alert("timegap can not be 0");
           	  return false;
           }

           switch(timeGap){
           	   case "month":
           	        timegap = month;
           	        break;
           	   case "day":
           	        timegap = day;
           	        break;
           	   case "hour":
           	        timegap = hour;
           	        break;
           }

           if(endDate.getTime() - startDate.getTime() < 0){
           	  alert("end date must be later then start date");
           	  return false;
           }



           var points = Math.floor((endDate.getTime() - startDate.getTime())/timegap);
           
           console.log("the value of points is:",points);
         
           
           var codinateXlabels = [];
           
           codinateXlabels.push(startDate.getTime());
            for(var i=1;i<=points;i++){
                codinateXlabels.push(startDate.getTime()+timegap*i - timegap/2);
            };

           codinateXlabels.push(endDate.getTime());
   
           this.$.codinateXlabels2 = codinateXlabels;

           var newCallData = [];
           
           for(var i=0; i< this.$.codinateXlabels2.length;i++){
           	   
           	   angular.forEach(this.$.callDataNew,function(v,k){
                   if(!newCallData[k]){
                   	  newCallData[k] = [];
                   }
                   /**
	           	   * generate total and compelete value for each agent
	           	   **/
	           	   var obj = {

	           	   }

	           	   var total = Math.round(Math.random()*100);
	           	   var compelete = Math.round(Math.random()*total);

	           	   obj.total = total;
	           	   obj.compelete = compelete;

	           	   newCallData[k].push(obj);

           	   });
           }

           this.$.callDataNew = newCallData;
           this.generateTotalAndCompeleteCalls();
           console.log(startDate,endDate,timeGap,timegap);
      },


      getNewObjectData:function(){
      	  console.log("second form submitted");
          console.log("the value of start time 2:",jQuery("#startDatepicker2").val());
          console.log("the value of start time 2:",jQuery("#endDatepicker2").val());
          console.log("the value of timeGap2",jQuery("#timegap2").val());
      }


	}

});