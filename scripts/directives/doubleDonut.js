"use strict";

angular.module("chart").directive("doubleDonut",function(){

    return {
    	restrict : "AE",
    	replace : true,
    	scope:{
    	  data: "=",
    	  size: "="
    	},
    	templateUrl : "views/doubleDonut.html",
    	link : function(scope,element,attr){
	        /**
	        * set the varible for the tips
	        **/
	        scope.currentTip = null;

            switch(scope.size){
            	case "large" :
              scope.config = {
  			        ROutO:116,
  			     	  ROutI:73,
  			        RInO:72,
  			        RInI:48,
  			        RInOCopy:72,
  			        RInICopy:48
		         };
		         break;

		        default :
             scope.config = {
			        ROutO:76,
			     	  ROutI:50,
			        RInO:48,
			        RInI:32,
			        RInOCopy:48,
			        RInICopy:32
		         };
		         break;
            }

          scope.config.defaultBorder = scope.size == "large" ? 3 : 1;
          var pieContainer = d3.select(element[0]);
            
	        var rotatePercent = 20;
	        var myScale = d3.scale.linear().domain([0, 100]).range([0, Math.PI*2]);
	        var colors = [
                "#da3a29",
                "#ee9a14",
                "#27ae60",
                "#d2d2d2",
                "#2493df",
                "#34495e"
	        ];

	        var generateRandomColor = function(){
	            var getRandom = function(){
	            	return Math.round(Math.random()*255);
	            }
	            var r = getRandom();
	            var g = getRandom();
	            var b = getRandom();
	            return "rgb("+r+","+g+","+b+")";
            };

            var getColor = function(index,startIndex){
                
                if(startIndex) {
                	index = parseInt(index) + startIndex;
                }

                if(!colors[index]){
                   colors[index] = generateRandomColor();
                }
                return colors[index];
            }

            /**
            * reset config, make inner dount bigger
            **/ 
            var resetConfigRadius = function(isZoomIn){
                if(isZoomIn){
                	scope.config.RInI = scope.config.ROutI;
                	scope.config.RInO = scope.config.ROutO - 7;
                }else{
                  scope.config.RInI = scope.config.RInICopy;
                	scope.config.RInO = scope.config.RInOCopy;	
                }

            }
            
            /**
            * function set path to 100 percent
            **/
            var setPathToFull = function(path){
                 path.chartInfo.start = 0;
                 path.chartInfo.end = 100;
                 if (path.position == "in") {
                 	 path.chartInfo.border = scope.size=="large" ? 8 : 5;
                 };
            }

            /**
            * function to set all other percentage to 0
            **/
            var setPathPercentagToZero = function(paths){                
                _.each(paths, function(v,k){
                      v.chartInfo.start = 0;
                      v.chartInfo.end = 0;
                      v.chartInfo.border = 0;
                });
            }

            /**
            * functon to recover all the paths
            **/
            var recoverPathsToOrigin = function(paths){
            	 _.each(paths,function(v,k){
                      v.chartInfo.start = v.chartInfo.originStart;
                      v.chartInfo.end = v.chartInfo.originEnd;
                      v.chartInfo.border = scope.config.defaultBorder;
            	 });
            }


            /**
            * function to extend original scope data
            **/
	               
	        var extendData = function(data,pos,startIndex){
                /**
                * get total value of scope data
                **/
                var sum = 0;

                _.each(data,function(v,k){
                	sum += v.value;
                });  
               	
               	for(var k in data){
                   var percentage = Math.round(data[k].value/sum*100);
                   var start = k-1>=0? data[k-1].chartInfo.end : 0;
                   var index = startIndex ? parseInt(k) + startIndex : parseInt(k);
                   angular.extend(data[k],{
                   	   id : index,
                   	   position : pos,
                       chartInfo:{
                       	   zoomIn : false,
                       	   originStart : start,
                           originEnd : start + percentage,
                       	   start : start,
                       	   end : start + percentage, 
                           color : getColor(index),
                           border : scope.config.defaultBorder
                       }
                   })
               	}        
	        }

            extendData(scope.data.basic,"out");
            extendData(scope.data.sub,"in",scope.data.basic.length);
            
            scope.arc = function(d,isOut){
            	if(isOut){
            		var radius={
            			out : scope.config.ROutO,
            			in : scope.config.ROutI
            		}
            	}else{
            	    var radius={
            	    	out: scope.config.RInO,
            	    	in : scope.config.RInI
            	    }
            	}

            	
            	var arc = d3.svg.arc()
            	.innerRadius(radius.in)
            	.outerRadius(radius.out)
            	.startAngle(function(){
            	  return myScale(d.chartInfo.start-rotatePercent);	
            	})
            	.endAngle(function(){
            	   return myScale(d.chartInfo.end-rotatePercent);
            	 });
            	return arc();
            
            };
     
           /**
           * call back function when mouse enter the pie chart
           **/

           scope.locateZoomInIcon = function(angleData,Ro,Ri,outside){
              
              if (scope.currentTip && scope.currentTip.chartInfo.zoomIn && scope.currentTip.id != angleData.id) {
              	  console.log("mouseover")
              	  return false;
              };
              
              scope.currentTip = angleData;
              
              if (outside) {
              	var startAngle = myScale(angleData.chartInfo.start-rotatePercent);
              	var endAngle = myScale(angleData.chartInfo.end-rotatePercent);
              }else{
              	var startAngle = myScale(angleData.chartInfo.start);
              	var endAngle = myScale(angleData.chartInfo.end);
              }

              var R = Math.abs(Ro-Ri)/2+Ri+1;
              var disAng = (endAngle - startAngle)/2; 
              var x = scope.config.ROutO + R*Math.sin(startAngle+disAng) - 9;
              var y = scope.config.ROutO - R*Math.cos(startAngle+disAng) - 74;

              pieContainer.select(".data-wrapper")
              .attr("style","left:"+x+"px;"+"top:"+y+"px;");
              pieContainer.select(".data-wrapper .data-detail")
              .attr("style","border-color:"+angleData.chartInfo.color);
              pieContainer.select(".data-wrapper")
              .classed("show",true);

         };
          
         /**
         * call back function when mouse leave the pie chart
         **/ 
         scope.mouseLeaveCallBack = function(e){
              try{
                   if(/data-wrapper|data-detail|glyphicon-zoom-in/.test(e.toElement.className)){
                    return false;
                  }
               }catch(e){
               	  console.log(e.message);
               }
              
               pieContainer.select(".data-wrapper")
               .classed("show",false);
         };

         /**
         * function to zoom in part of the donut
         **/
         scope.toogleZoomIn = function(){
              
              scope.currentTip.chartInfo.zoomIn = !scope.currentTip.chartInfo.zoomIn;
              
              var dataset = scope.currentTip.position == "out" ?
                            scope.data.basic : scope.data.sub;

              if(!scope.currentTip.chartInfo.zoomIn){
                  recoverPathsToOrigin(dataset);
                  if(scope.currentTip.position=="in"){
                  	resetConfigRadius(false);
                  }
                  return false;
              }

              var otherPaths = _.filter(dataset,function(v,k){
                  return v.id != scope.currentTip.id;
              });

              if(scope.currentTip.position == "in"){
              	 resetConfigRadius(true);
              }
              /**
              * set other path 
              **/
              setPathToFull(scope.currentTip);
              setPathPercentagToZero(otherPaths);

         }


     }
    }


});