"use strict";

angular.module("chart").directive("dxiMixChart",function(){
	
	return {
       restrict : "AE",
       replace : true,
       scope : {
         "data" : "="
       },
       templateUrl: "views/dxiMixChart.html",
       link : function(scope,ele,attr){       
           /**
           * should create the xscale and yscale, xAxis, yAxis, contextXscale, contextYscale
           * contextXais, then it will make easier for later
           **/
           var xscale,
               contextXScale,
               yscale,
               contextYscale,
               xAxis,
               yAxis,
               contextXscale,
               contextYscale,
               contextXais,
               lineColor = "#d2d2d2",
               brushBorderColor = "#2392df",
               colors = [
		          "#2392df",
		          "#ee9a14",
		          "#27ae60",
		          "#af5050",
		          "#9550af",
		          "#50afa7",
		          "#afa550",
		        ],
               colorsForTotal = [
	    			"#2392df",
	    			"#da3a29",
	    			"#27ae60",
	    			"#af5050",
	    			"#9550af",
	    			"#50afa7",
	    			"#afa550",
	                ],
               config = {
                  chartH : 300,
                  contextH: 60,
                  chartW : 996
               },
               padding = {
               	  top : 8,
               	  right: 40,
               	  left: 20,
               	  bottom : 8
               },
               xAxisHeight = 50,
               xAxisMarginTop = 10,
               defaultBarWidth = 10,
               currentBarWidth = 10,
               lines = {},
               contextLines = {},
               brush = null,
               svg = d3.select(ele[0]);
           
           var helper = {
           	   getTotalOfAllAgents : function(data){
		           var total={};
		           angular.forEach(data,function(v,k){
		                 var keys = Object.keys(v);
		                 if(!total[v.date]){
		                          total[v.date] = {
		                         }
		                      }
	                      keys.forEach(function(v2,k2){
	                             if(v2!="date" && v2!="label"){
	                                if(!total[v.date][v2]) total[v.date][v2] =0;
	                                total[v.date][v2] += v[v2];
	                             }
	                      });
		           })
		           return total;
		      },

		      convertObjectToArray : function(data){
                 var newData = [];
                 angular.forEach(data,function(v,k){
                      var copyV = angular.copy(v);
                      copyV.date = k;
                      newData.push(copyV); 
                 });
                 return newData;
		      },

              getAllDateExtent : function(data,needArray){
                 var date = [];
                 angular.forEach(data,function(v,k){
                     date.push(helper.getDateObject(k));
                 });
                 return needArray? date : d3.extent(date);
              },

		      getMaxOfAllTotalData:function(totalData){
		           var dataList = [];
		           angular.forEach(totalData,function(v,k){
		                angular.forEach(v,function(v2,k2){
		                    dataList.push(v2);
		                });
		           });
		           return d3.max(dataList);
		      },
		      generateRandomColor : function(){
		            var getRandom = function(){
		            	return Math.round(Math.random()*255);
		            }
		            var r = getRandom();
		            var g = getRandom();
		            var b = getRandom();
		            return "rgb("+r+","+g+","+b+")";
		      },
		      
		      getColor : function(index){
                   if(!colors[index]){
                   	   colors[index] = this.generateRandomColor();
                   }
                   return colors[index];
		      },

		      getDataKeys : function(data){
		      	    var keys = Object.keys(data);
                    keys = keys.filter(function(v,k){
                         return v!="date" && v!="label";
                    });
                    return keys;
		      },

		      setDataWrapper : function(x,y,color){

                svg.select(".data-wrapper")
                .classed("show",true)
                .attr("style",function(){
                    return "left:"+x + "px;" + "top:"+y + "px";
                })
                .select(".data-detail")
                .attr("style",function(){
                	var content = "border-color:" + color;
                	return content;
                });

		      },

		      transformRight : function(offsetX,domObj){
                   domObj.attr("transform","translate("+offsetX+",0)");
              },

              transformLeft : function(offsetX,domObj){
		           var pixelToMoveLeft = offsetX - config.chartW;
		           domObj.attr("transform","translate("+pixelToMoveLeft+",0)");
              },

              getDateObject : function(dateStr){
              	  if(typeof dateStr == "string" && /^\d+$/.test(dateStr)){
                     	dateStr = parseInt(dateStr);
                  }
                  return new Date(dateStr);
              },

              getNumberInDateRange : function(startTime,endTime,dateArr){
                  var start = startTime.getTime();
                  var end = endTime.getTime();

                  var matchDate = _.filter(dateArr,function(v,k){
                      return v.getTime() >= start && v.getTime() <= end;
                  });
                  return matchDate.length;
              }


           };


           var chart = {
               
               setUpChartEnvironment : function(){
               	    var totalData = scope.total = helper.getTotalOfAllAgents(scope.data);
               	    var maxTotal = helper.getMaxOfAllTotalData(totalData);
                    var maxTotalLength = maxTotal.toString().length;
               	    padding.left = maxTotalLength == 1 ? 9 : maxTotalLength * 8;
               	    
               	    scope.dateArr = helper.getAllDateExtent(totalData,true);
               	    scope.totalArr = helper.convertObjectToArray(totalData)
               	    
               	    var extent = d3.extent(scope.dateArr);
               	    
               	    xscale = d3.time.scale().domain(extent).range([padding.left,config.chartW - padding.right]);
               	    contextXscale = d3.time.scale().domain(extent).range([padding.left,config.chartW - padding.right]);

               	    yscale = d3.scale.linear().domain([maxTotal,0]).range([padding.top, config.chartH]);
                    contextYscale = d3.scale.linear().domain([maxTotal,0]).range([0,config.contextH])

                    xAxis = d3.svg.axis()
                            .scale(xscale)
                            .tickSize(-config.chartH)
                            .orient("bottom");
                    yAxis = d3.svg.axis()
                            .scale(yscale)
                            .tickSize(-config.chartW)
                            .orient("left");
                    
                    this.setSvgWidth();
                    this.setSvgHeight();
                    this.setUpBarWidth();
                    this.buildChartAxis();
                    this.setUpBardata();
                    this.setUpLines();
                    this.setUpBrush();
                    
                    contextXais = d3.svg.axis().scale(xscale).orient("bottom");
               },

               buildChartAxis : function(coordinate){
                    if(coordinate ==  "x" || !coordinate){
                       svg.select(".axis.x")
                       .attr("transform","translate(0,"+(config.chartH+xAxisMarginTop)+")")
                       .call(xAxis);
                    }

                    if(coordinate ==  "y" || !coordinate){
                       svg.select(".axis.y")
                       .attr("transform","translate("+(padding.left+1)+",0)")
                       .call(yAxis);
                    }
               },

               setSvgWidth : function(){
               	    scope.svgWidth = config.chartW;
               },

               setSvgHeight: function(){
                    scope.svgHeight = config.chartH + xAxisHeight*2 + config.contextH;
               },

               setUpBarWidth : function(){
                    var extent = xscale.domain();
                    var matchPointsNumber = helper.getNumberInDateRange(extent[0],extent[1],scope.dateArr);
                    /**
                    * get number of agent
                    **/
                    var agentsNum = scope.data.length/scope.dateArr.length;
                    /**
                    * get key number
                    **/
                    var dataKeys = helper.getDataKeys(scope.data[0]);
                    var keyNumber = dataKeys.length;
                    var barNumber = agentsNum * keyNumber * matchPointsNumber;
                    var maxBarWidth = (config.chartW - padding.left - padding.right)/barNumber;
                    currentBarWidth = maxBarWidth > defaultBarWidth ? defaultBarWidth : maxBarWidth;

                    if(currentBarWidth >1 && currentBarWidth < defaultBarWidth){
                    	currentBarWidth = currentBarWidth -1;
                    }
               },

               setUpBardata : function(){

                    var barData = {

                    }

                    angular.forEach(scope.data,function(v,k){
                        if(!barData[v.date]){
                           barData[v.date] = [];	
                        }
                        
                        var newV = {};
                        
                        angular.forEach(v,function(v2,k2){
                             if(k2!="date" && k2!="label"){
                                if(!newV["values"]) newV["values"]={};
                                newV["values"][k2] = {"value" : v2};
                             }else{
                             	newV[k2] = v2; 
                             } 
                        });

                        barData[v.date].push(newV);  
                    });

                    /**
                    * sort the bar data
                    **/

                    angular.forEach(barData,function(v,k){
                        barData[k] = _.sortBy(v,"label");
                    });

                    /**
                    * set color for each bar
                    **/

                    angular.forEach(barData,function(v,k){
                        angular.forEach(v,function(v2,k2){
                           var keys = Object.keys(v2.values);
                           angular.forEach(v2.values,function(v3,k3){
                               var index = k2*keys.length + keys.indexOf(k3)
                               v3.color = helper.getColor(k2*keys.length + keys.indexOf(k3));
                               v3.x = xscale(new Date(v2.date)) + currentBarWidth*index;
                               v3.y = yscale(v3.value);
                               v3.width = currentBarWidth;
                               v3.height = config.chartH - yscale(v3.value);   
                           });
                        });
                    });

                    scope.barData = barData;
               },

               updateBarChart : function(){
               	   angular.forEach(scope.barData,function(v,k){
                        angular.forEach(v,function(v2,k2){
                           var keys = Object.keys(v2.values);
                           angular.forEach(v2.values,function(v3,k3){
                           	   var index = k2*keys.length + keys.indexOf(k3)
                               v3.x = xscale(new Date(v2.date)) + currentBarWidth*index;
                               v3.width = currentBarWidth;     
                           });
                        });
                    });
               },

               setUpLines : function(){
                    if(!scope.data.length) return false;
                    var keys = helper.getDataKeys(scope.data[0]);

                    keys.forEach(function(v,k){
                    	lines[k] = d3.svg.line()
                    	           .x(function(d){
                    	           	  return xscale(helper.getDateObject(d.date));
                    	           })
                    	           .y(function(d){
                    	           	  return yscale(d[v]);
                    	           });

                    	 contextLines[k] = d3.svg.line()
                    	                   .x(function(d){
                    	                   	  return contextXscale(helper.getDateObject(d.date));
                    	                   })
                    	                   .y(function(d){
                    	                   	  return  contextYscale(d[v]);
                    	                   }); 
                    });
               },

               setUpBrush : function(){
                   
                   var that = this;
                   

                   brush = d3.svg.brush()
			          .x(contextXscale)
			          .on("brush", function(){
			              
			              var extent = brush.extent();
			              xscale.domain(brush.empty()? contextXscale.domain() : extent);
			              
			              var context = svg.select(".context");
			              var leftBrushRect = context.select(".outside-brush").select("rect:first-child");
			              var rightBrushRect = context.select(".outside-brush").select("rect:last-child");
			              var leftBorderLine = context.select(".outside-brush-border").select("line:first-child");
			              var rightBorderLine = context.select(".outside-brush-border").select("line:last-child");
			              
			              if(brush.empty()){
			                helper.transformRight(0,leftBrushRect);
			                helper.transformLeft(config.chartW,rightBrushRect);
			                helper.transformRight(0,leftBorderLine);
			                helper.transformLeft(config.chartW,rightBorderLine);
			              }else{
			                helper.transformRight(contextXscale(extent[0]),leftBrushRect);
			                helper.transformLeft(contextXscale(extent[1]),rightBrushRect);
			                helper.transformRight(contextXscale(extent[0]),leftBorderLine);
			                helper.transformLeft(contextXscale(extent[1]),rightBorderLine);
			              }
                          that.setUpBarWidth();
                          that.buildChartAxis("x");
                          that.updateBarChart();
                          that.buildPaths();
                          that.buildDots();
                          scope.$digest();
			          })
			          .on("brushend",function(){			              
			          });
      
               },

               build :function(){
               	   this.buildPaths();
               	   this.buildDots();
               	   this.buildExtent();
               },

               buildPaths : function(){

                   svg.select(".path-group").selectAll("path").remove();
                   
                   angular.forEach(lines,function(v,k){
                        svg.select(".path-group").append("path")
                       .datum(scope.totalArr)
                       .attr("d",v)
                       .attr("stroke",helper.getColor(k))
                       .attr("stroke-width",1)
                       .attr("fill","transparent");
                   });
                
               },



               buildExtent : function(){
                   var context = svg.select(".context")
                   .attr("transform","translate(0,"+(config.chartH+xAxisHeight)+")");
                    
                    context.select(".border-lines")
                    .selectAll("line")
                    .data([0,1])
                    .enter()
                    .append("line")
                    .attr("x1",0)
                    .attr("y1",function(d){
                    	return d*config.contextH;
                    })
                    .attr("x2",config.chartW)
                    .attr("y2",function(d){
                    	return d*config.contextH;
                    })
                    .attr("stroke",lineColor);
                    
                    /**
                    * remove all the old lines
                    **/
                    context.select(".split-lines")
                    .selectAll("line")
                    .remove();

                    /**
                    * build the splite lines
                    **/
                    context.select(".split-lines")
                    .selectAll("line")
                    .data(scope.dateArr)
                    .enter()
                    .append("line")
                    .attr("x1",function(d){
                        return contextXscale(d);
                    })
                    .attr("y1",0)
                    .attr("x2",function(d){
                    	return contextXscale(d);
                    })
                    .attr("y2",config.contextH)
                    .attr("stroke",lineColor)
                    .attr("stroke-dasharray","8,8");

                    
                    /**
                    * build the brush
                    **/
					context.select(".x.brush")
					.call(brush)
					.selectAll("rect")
					.attr("y", 0)
					.attr("height", config.contextH)
					.attr("fill","transparent");
                   
                    /**
                    * build xais
                    **/
                    context.select(".x.axis")
                    .attr("transform","translate(0," + config.contextH+")")
                    .call(contextXais);
                    
                    /**
                    * remove all the paths
                    **/
                    context.select(".path-group").selectAll("path").remove();


                    /**
                    * build paths
                    **/
                    angular.forEach(contextLines,function(v,k){
                       context.select(".path-group").append("path")
                       .datum(scope.totalArr)
                       .attr("d",v)
                       .attr("stroke",helper.getColor(k))
                       .attr("stroke-width",1)
                       .attr("fill","transparent");
                    });
 
                    /**
                    * build the outside brush
                    **/
					context.select(".outside-brush")
					.selectAll("rect")
					.data([-1,1])
					.enter()
					.append("rect")
					.attr("x",function(d,i){
					  return d*config.chartW
					})
					.attr("y",0)
					.attr("width",config.chartW)
					.attr("height",config.contextH)
					.attr("style","fill:rgba(255,255,255,0.5)");

					/**
					* build the border for the outside brush
					**/
                    context.select(".outside-brush-border")
                    .selectAll("line")
                    .data([0,1])
                    .enter()
                    .append("line")
                    .attr("x1",function(d){
                        return d*config.chartW;
                    })
                    .attr("y1",0)
                    .attr("x2",function(d){
                        return d*config.chartW;
                    })
                    .attr("y2",config.contextH)
                    .attr("stroke", brushBorderColor)
                    .attr("stroke-width",2);

               },

               buildDots : function(){
                  
                  svg.select(".dots-group").selectAll("circle").remove();
                  
                  var keys = helper.getDataKeys(scope.data[0]);

                  angular.forEach(keys,function(v,k){
                      
                      angular.forEach(scope.total,function(v2,k2){
                            svg.select(".dots-group")
                            .append("circle")
                            .attr("cx", function(){
                            	return xscale(helper.getDateObject(k2));
                            })
                            .attr("cy",function(){
                            	return yscale(v2[v]);
                            })
                            .attr("r","4")
                            .attr("fill",helper.getColor(k))
                            .on("mouseenter",function(){
                                 
                                 scope.curretAgent={
                                 	 label : "Total",
                                 	 key : v,
                                 	 value : v2[v]
                                 }

                                 scope.$digest();

                                 helper.setDataWrapper(d3.event.offsetX,d3.event.offsetY,helper.getColor(k));

                            })
                            .on("mouseleave",function(){
                                scope.mouseLeaveCallBack();
                            });
                      });

                  });

               }
           }


           scope.mouseLeaveCallBack = function(){
                svg.select(".data-wrapper")
                .classed("show",false)
           };

           scope.mouseOverCallBack = function(agent,bar,key,e){
                scope.curretAgent = {
                  label : agent.label,
                  key : key,
                  value : bar.value
                };
                helper.setDataWrapper(e.offsetX,e.offsetY,bar.color); 
           };


           scope.$watch("data",function(newVal,oldVal){
                if(newVal){
                	chart.setUpChartEnvironment();
                	chart.build();
                }
           });

       }
	}
  
});