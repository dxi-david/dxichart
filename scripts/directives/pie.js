/**
* directive for pie chart
**/

"use strict";

angular.module("chart").directive("pie",function(){
	return {
       restrict:"AE",
       replace:true,
       templateUrl:"views/pie.html",
       scope:{
       	 "data":"=",
         "setting":"=",
       },
       link:function(scope,element,attr){
        var colors = [
          "#2392df",
          "#ee9a14",
          "#27ae60",
          "#af5050",
          "#9550af",
          "#50afa7",
          "#afa550",
        ];
        
        scope.width = scope.setting.width;
        scope.height = scope.setting.height;

        var translateDis = 10;
        var radius = Math.min(scope.width - translateDis * 2 , scope.height - translateDis *2 ) / 2;
        var rootContainer = d3.select(element[0]);
        
        var pie = d3.layout.pie()
          .value(function(d) { return d.count; })
          .sort(null);


        var generateRandomColor = function(){
            var getRandom = function(){
              return Math.round(Math.random()*255);
            }
            var r = getRandom();
            var g = getRandom();
            var b = getRandom();
            return "rgb("+r+","+g+","+b+")";
        };

        scope.getColor = function(index){
          if(!colors[index]){
              colors[index] = generateRandomColor();
          }
          return colors[index];
        }

       scope.arc = d3.svg.arc()
          .outerRadius(radius)
          .innerRadius(scope.setting.innerRadius);
       
       scope.colors = colors;

       scope.dataDetail={
             color:"#2392df",
             value:70,
             label:"",
             x:0,
             y:0
        }

        scope.mouseEnterCallBack = function(d,e,index){ 

              var angle = d.startAngle + (d.endAngle-d.startAngle)/2;
              var translateY= - Math.round(translateDis * Math.cos(angle));
              var translateX = Math.round(translateDis * Math.sin(angle));
              d3.select(e.target)
              .attr("transform","translate("+translateX+","+translateY+")");

             rootContainer.select("#call-detail")
             .classed("hideChartElement",false);

              scope.dataDetail.value = d.value;
              scope.dataDetail.color = colors[index];
              scope.dataDetail.label = scope.data[index].label;
          
        };

        scope.mouseLeaveCallBack = function(e){
             rootContainer.select("#call-detail").classed("hideChartElement",true);
             d3.select(e.target).attr("transform","translate(0,0)");
        } 

        scope.$watch("data",function(newVal,oldVal){
            scope.pieData = pie(scope.data);
        },true);

	  }
  }
});
